package com.glearning.orders.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glearning.orders.model.User;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long>{
	
	Optional<User> findByEmail(String name);

}
