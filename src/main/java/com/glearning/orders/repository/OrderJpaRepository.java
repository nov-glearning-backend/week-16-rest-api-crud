package com.glearning.orders.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.glearning.orders.model.Order;
import com.glearning.orders.model.OrderDto;

@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long> {

	Page<OrderDto> findByPriceBetween(double min, double max, Pageable pageRequest);

	@Query("select o from Order o where o.email = ?1")
	Optional<Order> findByEmail(String email);

}
