package com.glearning.orders.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.glearning.orders.model.User;
import com.glearning.orders.repository.UserJpaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserService {
	
	private final UserJpaRepository userRepository;
	
	public Set<User> listUsers(){
		return Set.copyOf(this.userRepository.findAll());
	}
	
	public User getUserById(long id) {
		return this.userRepository
					.findById(id)
					.orElseThrow(() -> new IllegalArgumentException("invalid user id passed"));
	}
	
	public User saveUser(User user) {
		return this.userRepository.save(user);
	}

}
