package com.glearning.orders.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.glearning.orders.model.DomainUserDetails;
import com.glearning.orders.repository.UserJpaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {
	
	private final UserJpaRepository repository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return this.repository.findByEmail(email)
							  .map(DomainUserDetails::new)
							  .orElseThrow(() -> new UsernameNotFoundException("invalid email address passed"));
	}
}
