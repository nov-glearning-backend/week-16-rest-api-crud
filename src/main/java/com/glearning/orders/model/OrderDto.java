package com.glearning.orders.model;

import java.time.LocalDate;

public interface OrderDto {
	
	String getCustomerName();
	double getPrice();
	String getEmail();
	LocalDate getOrderDate();
}
